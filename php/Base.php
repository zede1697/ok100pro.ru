<?php
/**
 * Created by PhpStorm.
 * User: Zede
 * Date: 10.01.2016
 * Time: 12:57
 */

namespace
{
	require_once 'SQL.php';
	require_once 'Session.php';
	require_once 'CodeGenerator.php';
	require_once 'JsonGenerator.php';

	if($_SERVER['SERVER_ADDR'] === '127.0.0.1')
			SQLWorker::ActivateSQL('localhost', 'mysql', 'mysql', 'test');
	else
		SQLWorker::ActivateSQL('mysql.hostinger.ru', 'u930522322_100pr', 'q2w3q2w3', 'u930522322_100pr');
	Session::Start();

	function all_is_set($params, $array)
	{
		foreach($params as $val)
			if(!isset($array[$val]))
				return false;
		return true;
	}
}

namespace HtmlGen
{
	function GenLoadingPanel()
	{
		return '<div id=\'loadingPanel\'>Загрузка<div><div></div><div></div><div></div></div></div>';
	}

	function GenMenuPanel()
	{
		return '<div id=\'main-menu\'><div><header><p></p><button>&#10006;</button></header><div></div></div></div>';
	}

	function GetOwnDataForm($dataJson)
	{
		$dataJson = array_merge($dataJson, ['session_active' => var_export(\Session::IsInitialized(), true)]);
		$moduleJs = <<<'JS'
var
	module = new sys.modules.Module('link');
module.onquery = function(e)
	{
		var
			tagLi = e.target,
			href = tagLi.getElementsByTagName('a')[0].getAttribute('href');
		sys.linkClick(href, e.which);
	};
return module;
JS;
		$moduleDiv = GenDiv(['data-key' => 'link'])->
			StrictPush(GenInput('hidden', StringToHtmlAttrFormat($moduleJs), ['class' => 'data-js']));
		$form = new TagComplexDecorator('div', ['id' => 'data']);
		return $form->StrictPush([
				GenInput('hidden', \JsonGen\GenJsonAttrFormat($dataJson), ['id' => 'data-json']),
				$moduleDiv,
				new TagComplexDecorator('form', ['name' => 'data-form'])
			]);
	}

	function GenOwnFooter()
	{
		$footer = new TagComplexDecorator('footer', ['id' => 'nav-footer']);
		return $footer;
	}

	function GenOwnHeader()
	{
		$header = new TagComplexDecorator('header');
		$backContainer = GenDiv(['id' => 'headerBack']);
		$backContainer->StrictPush(GenImg('images/headerBackground.jpg', 'Фоновое изображение', ['class' => 'no-drag']));
		$liStamp = new TagStamp('li', [], ['data-access', 'data-type']);
		$mainMenu = new TagComplexDecorator('ul');
		$subMenu = new TagComplexDecorator('ul', ['data-type' => 'sub-menu']);

		$subMenu->StrictPush($liStamp->ComplexGenerate([
				[['link', 'desk'], GenA('Юридические отдел', '/page/legal_department')],
				[['link', 'desk'], GenA('Палата экспертов', '/page/chamber_experts')],
				[['link', 'desk'], GenA('Аварийные комиссары', '/page/commissars')],
				[['link', 'desk'], GenA('Мини-эвакуатор', '/page/mini-evacuator')]
			]));

		$liMenu = $liStamp->ComplexGenerate([
				[['profile', 'desk'], 'Профиль'],
				[['none', 'desk'], ''], //это хак для поддержания чистоты :nth-child(even)
				[['enter', 'desk'], 'Вход'],
				[['link', 'desk'], GenA('Наши контакты', '/page/contacts')],
				[['sub-menu', 'desk'], 'Услуги'],
				[['link', 'desk'], GenA('Нашим агентам', '/page/agents')],
				[['link', 'desk'], GenA('Автосервисы', '/page/garages')],
				[['link', 'desk'], GenA('Вопрос - Ответ', '/page/question_answer')],
				[['menu', 'mob'], 'Меню'],
				[['nav', 'mob'], 'Навигация'],
				[['profile', 'mob'], 'Профиль'],
				[['enter', 'mob'], 'Вход']
			]);
		$mainMenu->StrictPush($liMenu);
		$navContainer = GenDiv(['id' => 'headerNav']);
		$navContainer->StrictPush([$mainMenu, $subMenu]);
		$header->StrictPush([$backContainer, $navContainer]);
		return $header;
	}

	function GenOwnMainLine()
	{
		return new TagComplexDecorator('main');
	}

	//<editor-fold desc="GenSections">
	function GenSectionEnter()
	{
		$moduleJs = <<<'JS'
var
	module = new sys.modules.Module('enter');
module.emailTag = null;
module.passTag = null;
module.data.email = '';
module.data.pass = '';
module.data.inputOnChange = function()
	{
		document.getElementById('data-enter-output').innerHTML = '';
	};

module.onquery = function()
	{
		sys.menu.open("enter");
	};

module.onclose = function()
	{
		 module.data.email = this.emailTag.value;
		 module.data.pass = this.passTag.value;
	};

module.onopened = function()
	{
		this.emailTag = this.space.querySelector('input[type=email]');
		this.passTag = this.space.querySelector('input[type=password]');
		this.space.querySelector('input[value=Войти]').onclick = function()
			{
				var
					email = sys.menu.currentModule.emailTag.value,
					password = sys.menu.currentModule.passTag.value;
				sys.script('user.logIn', {email: email, password: password}, function(res)
					{
						var
							responce = JSON.parse(res);
						if(responce.status === 'correct')
						{
							if(responce.result === 'true')
							{
								sys.state.session_active = true;
								document.body.setAttribute('data-init', 'true');
								sys.menu.close();
							}else
							{
								document.getElementById('data-enter-output').innerHTML = 'Несуществующая комбинация логина и пароля';
							}
						}else
						{
							console.log(responce.result);
						}
					});
			};
		this.space.querySelector('input[value=Зарегистрироваться]').onmouseup = function(e)
			{
				sys.linkClick('/page/registration', e.which);
			};
		this.emailTag.value = module.data.email;
		this.emailTag.oninput = this.data.inputOnChange;
		this.passTag.value = module.data.pass;
		this.passTag.oninput = this.data.inputOnChange;
	};

return module;
JS;
		return GenDiv(['data-key' => 'enter'])->StrictPush([
				'<header>Вход на сайт</header>',
				GenInput('hidden', StringToHtmlAttrFormat($moduleJs), ['class' => 'data-js']),
				GenDiv()->StrictPush([
						GenP('Зарегистрированным:'),
						GenInput('email', '', ['form' => 'data-form', 'name' => 'email', 'placeholder' => 'Введите e-mail', 'autocomplete' => 'on']),
						GenInput('password', '', ['form' => 'data-form', 'name' => 'password', 'placeholder' => 'Введите пароль', 'autocomplete' => 'on']),
						GenInput('button', 'Войти'),
						GenDiv(['id' => 'data-enter-output']),
						GenHr(),
						GenP('Вы еще не зарегистрированы?'),
						GenInput('button', 'Зарегистрироваться')
					])
			]);
	}

	function GenSectionMenu()
	{
		$moduleJs = <<<'JS'
var
	module = new sys.modules.Module('menu');
module.onquery = function()
	{
		sys.menu.open("menu");
	};
return module;
JS;
		return GenDiv(['data-key' => 'menu'])->StrictPush([
				'<header>Навигация по сайту</header>',
				GenInput('hidden', StringToHtmlAttrFormat($moduleJs), ['class' => 'data-js']),
				GenDiv()->StrictPush((new TagComplexDecorator('nav'))->StrictPush([
				GenA('Наши контакты','/page/contacts'),
				GenP('Наши услуги', ['data-type' => 'comboItem']),
				GenDiv()->StrictPush([
					GenA('Юридический отдел','/page/legal_department'),
					GenA('Палата экспертов','/page/chamber_experts'),
					GenA('Аварийные комиссары','/page/commissars'),
					GenA('Мини-эвакуатор','/page/mini-evacuator'),
				]),
				GenA('Нашим агентам', '/page/agents'),
				GenA('Автосервисы 100PRO', '/page/garages'),
				GenA('Вопрос - ответ', '/page/question_answer'),
			]))
		]);
	}

	function GenSectionNav()
	{
		$moduleJs = <<<'JS'
var
	module = new sys.modules.Module('nav');
module.onquery = function()
	{
		sys.menu.open("nav");
	};
return module;
JS;
		return GenDiv(['data-key' => 'nav'])->StrictPush([
					'<header>Навигация по странице</header>',
					GenInput('hidden', StringToHtmlAttrFormat($moduleJs), ['class' => 'data-js']),
					GenDiv()->StrictPush((new TagComplexDecorator('nav'))->StrictPush([
						GenA('Наверх','#'),
						GenA('Вниз','#nav-footer')
					]))
			]);
	}

	function GenSectionProfile()
	{
		$moduleJs = <<<'JS'
var
	module = new sys.modules.Module('nav');
module.onquery = function()
	{
		sys.menu.open("profile");
	};
module.onopened = function()
	{
		this.space.querySelector('input[value=Выйти]').onclick = function()
			{
				sys.script('user.logOut', {}, function()
					{
						sys.state.session_active = false;
						document.body.setAttribute('data-init', 'false');
						sys.menu.close();
					});
			};
	};
return module;
JS;
		return GenDiv(['data-key' => 'profile'])->StrictPush([
				'<header>Меню профиля</header>',
				GenInput('hidden', StringToHtmlAttrFormat($moduleJs), ['class' => 'data-js']),
				GenDiv()->StrictPush([
					GenInput('button', 'Выйти')
					])
			]);
	}
	//</editor-fold>
}