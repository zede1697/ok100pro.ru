<?php

/**
 * Created by PhpStorm.
 * User: Zede
 * Date: 22.01.2016
 * Time: 7:38
 */
namespace
{
	require_once('Base.php');
}


namespace JsonGen
{
	class JsonStamp
	{
		protected $keyStamp;
		protected $keyBase;

		public function __construct($keyBase = [], $keyStamp = [])
			{
				$this->keyBase = $keyBase;
				$this->keyStamp = $keyStamp;
			}

		public function GenJsonArray($dataStamp = [], $dataOther = [])
			{
				$filledKeyStamp = ($this->keyStamp != []) ? array_combine($this->keyStamp, $dataStamp) : [];
				return array_merge($this->keyBase, $filledKeyStamp, $dataOther);
			}

		public function Generate($dataStamp = [], $dataOther = [])
			{
				return json_encode($this->GenJsonArray($dataStamp, $dataOther), JSON_FORCE_OBJECT);
			}
	}

	function GenError($message, $other = [])
	{
		$jsonPre = ['status' => 'error', 'result' => $message];
		if(is_array($other))
			$jsonPre = array_merge($jsonPre, $other);
		return json_encode($jsonPre);
	}

	function GenJsonAttrFormat($data)
	{
		return \HtmlGen\StringToHtmlAttrFormat((is_string($data))? $data : json_encode($data, JSON_FORCE_OBJECT));
	}
}