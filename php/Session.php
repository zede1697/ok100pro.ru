<?php
/**
 * Created by PhpStorm.
 * User: Zede
 * Date: 10.01.2016
 * Time: 13:23
 */

require_once 'SQL.php';

session_start();

class Session
{
	const STATUS_UNINITIALIZED = 0;
	const STATUS_INITIALIZED = 1;
	const STATUS_FAKE = 2;

	protected static $val;

	static public function Start()
	{
		if(isset($_SESSION['user']))
		{
			$ip = $_SERVER["REMOTE_ADDR"];
			if($_SESSION['hash'] !== md5($ip . $_SESSION['user']))
				self::$val = self::STATUS_FAKE;
			else
				self::$val = self::STATUS_INITIALIZED;
		}else
			self::$val = self::STATUS_UNINITIALIZED;
		return self::$val;
	}

	static public function Initialization($id)
		{
			$_SESSION['user'] = $id;
			$_SESSION['hash'] = md5($_SERVER["REMOTE_ADDR"] . $_SESSION['user']);
		}

	static public function GetStatus()
	{
		return self::$val;
	}

	static public function IsInitialized()
		{
			return self::$val == self::STATUS_INITIALIZED;
		}

	static public function GetUserID()
		{
			return $_SESSION['user'];
		}

	static public function Reset()
		{
			unset($_SESSION['hash']);
			unset($_SESSION['user']);
		}
}