<?php

/**
 * Created by PhpStorm.
 * User: Zede
 * Date: 11.01.2016
 * Time: 12:49
 */
namespace
{
	require_once 'SQL.php';
	require_once 'Session.php';
}

namespace HtmlGen
{
	abstract class CodeGenerator
	{
		protected $attributes = [];

		public function __construct($attributes = null)
			{
				if(is_array($attributes))
					$this->attributes = $attributes;
			}

		public abstract function Generate();

		public function SetAttributes($attributes)
			{
				$this->attributes = $attributes;
			}

		public function PushAttributes($attributes)
			{
				$this->attributes = array_merge($this->attributes, $attributes);
			}

		public function SetID($id)
			{
				$this->attributes['id'] = $id;
			}

		public function SetClass($class)
		{
			$this->attributes['class'] = $class;
		}

		public function ComplexAttributes()
			{
				$array = [];
				if(!is_array($this->attributes))
					return '';
				foreach($this->attributes as $attr => $val)
					$array[] = ($val !== '')? "$attr='$val' " : "$attr ";
				return implode($array);
			}
	}

	class TagDecorator extends CodeGenerator
	{
		protected $tag;

		public function __construct($tag, $attributes = null)
		{
			parent::__construct($attributes);
			$this->tag = $tag;
		}

		public function Generate()
		{
			return "<$this->tag ".$this->ComplexAttributes()."/>";
		}
	}

	abstract class ComplexCodeGenerator extends CodeGenerator
	{
		protected $bufferArray;
		protected $arrayOfParts = [];

		public function Push($val)
			{
				if(is_array($val))
					$this->arrayOfParts = array_merge($this->arrayOfParts, $val);
				else
					$this->arrayOfParts[] = $val;
				return $this;
			}

		public function StrictPush($val)
			{
				if(is_array($val))
				{
					foreach($val as $param)
						$param = (is_string($param))? $param : $param->Generate();
					$this->arrayOfParts = array_merge($this->arrayOfParts, $val);
				}else
					$this->arrayOfParts[] = (is_string($val))? $val : $val->Generate();
				return $this;
			}

		public function Complex()
			{
				unset($this->bufferArray);
				if(!isset($this->arrayOfParts[0]))
					return '';
				foreach($this->arrayOfParts as $param)
					$this->bufferArray[] = (is_string($param))? $param : $param->Generate();
				return implode($this->bufferArray);
			}
	}

	class TagComplexDecorator extends ComplexCodeGenerator
	{
		protected $tag;

		public function __construct($tag, $attr = null)
		{
			parent::__construct($attr);
			$this->tag = $tag;
		}

		public function Generate()
		{
			return "<$this->tag ".$this->ComplexAttributes().'>'.$this->Complex()."</$this->tag>";
		}
	}

	class TagStamp
	{
		protected $tag;
		protected $attrStamp;
		protected $attrBase;
		protected $separator;

		protected function ComplexAttributes($addAttr = [])
		{
			$array = [];
			$filledStampAttr = ($this->attrStamp != []) ? array_combine($this->attrStamp, $addAttr[0]) : [];
			unset($addAttr[0]);
			if(isset($addAttr[1]))
				unset($addAttr[1]);
			$bufferAttributes = array_merge($this->attrBase, $filledStampAttr, $addAttr);
			foreach($bufferAttributes as $attr => $val)
				$array[] = ($val !== '') ? "$attr='$val' " : "$attr ";
			return implode($this->separator, $array);
		}

		protected function ComplexContent($content)
		{
			if(is_array($content))
			{
				$bufferArray = [];
				foreach($content as $param)
					$bufferArray[] = (is_string($param)) ? $param : $param->Generate();
				return implode($bufferArray);
			}else
				return (is_string($content))? $content : $content->Generate();
		}

		public function __construct($tag, $attrBase = [], $attrStamp = [], $separator = '')
		{
			$this->tag = $tag;
			$this->attrStamp = $attrStamp;
			$this->attrBase = $attrBase;
			$this->separator = (is_string($separator))? $separator : $separator->Generate();
		}

		public function Generate($data)
		{
			$buffer = [];
			foreach($data as $clone)
			{
				$bufferStr = "<$this->tag ".($this->ComplexAttributes($clone))."/>";//"
				$buffer[] = $bufferStr;
			}
			return implode($buffer);
		}

		public function SGenerate($data)
			{
				return "<$this->tag ".($this->ComplexAttributes($data))."/>";
			}

		public function ComplexGenerate($data)
		{
			$buffer = [];
			foreach($data as $clone)
			{
				$content = $this->ComplexContent($clone[1]);
				$attr = $this->ComplexAttributes($clone);
				$bufferStr = "<$this->tag $attr>$content</$this->tag>";
				$buffer[] = $bufferStr;
			}
			return implode($buffer);
		}

		public function SComplexGenerate($data)
			{
				$content = $this->ComplexContent($data[1]);
				$attr = $this->ComplexAttributes($data);
				return "<$this->tag $attr>$content</$this->tag>";
			}
	}

	class Formater
	{
		protected $formatStr;

		public function __construct($formatStr)
		{

		}
	}

	class TagHead extends ComplexCodeGenerator
	{
		protected $title;
		protected $css;

		public function __construct($title, $css = [])
			{
				$this->title = $title;
				$this->css = $css;
			}

		public function PushLess($less)
			{
				if(is_string($less))
					$this->Push("<link rel='stylesheet/less' type='text/css' href='style/$less' />");
				else
				{
					$array = [];
					foreach($less as $element)
						$array[] = "<link rel='stylesheet/less' type='text/css' href='style/$element' />";
					$this->Push(implode($array));
				}
			}

		public function PushCss($css)
			{
				if(is_string($css))
					$this->Push("<link rel='stylesheet' href='style/$css' />");
				else
				{
					$array = [];
					foreach($css as $element)
						$array[] = "<link rel='stylesheet' href='style/$element' />";
					$this->Push(implode($array));
				}
			}

		public function Generate()
			{
				return '<head>'."<title>$this->title</title>".$this->Complex().'</head>';
			}
	}

	class TagBody extends ComplexCodeGenerator
	{
		public function Generate()
		{
			return '<body '.$this->ComplexAttributes().'>'.$this->Complex().'</body>';
		}
	}

	class TagSimpleUlGen extends ComplexCodeGenerator
	{
		public function Complex()
		{
			unset($this->bufferArray);
			if(!isset($this->arrayOfParts[0]))
				return '';
			foreach($this->arrayOfParts as $param)
				$this->bufferArray[] = '<li>'.((is_string($param))? $param : $param->Generate()).'</li>';
			return implode($this->bufferArray);
		}

		public function Generate()
		{
			return "<ul ".$this->ComplexAttributes().'>'.$this->Complex()."</ul>";
		}
	}

	class GeneratorHTML extends ComplexCodeGenerator
	{
		public function Generate()
		{
			return '<!DOCTYPE html><html>'.$this->Complex().'</html>';
		}
	}

	function DocumentFabric($head = '', $body = '', $postBodyJS = '')
	{
		$generator = new GeneratorHTML();
		$generator->Push($head);
		$generator->Push($body);
		if(is_string($postBodyJS))
		{
			if($postBodyJS !== '')
			{
				if(strpos($postBodyJS, "http") != false)
					$postBodyJS = 'js/'.$postBodyJS;
				$generator->Push("<script type='text/javascript' src='js/$postBodyJS' ></script>");
			}
		}else
		{
			$array = [];
			foreach($postBodyJS as $element)
			{
				if(strpos($element, "http") !== 0)
					$element = 'js/'.$element;
				$array[] = "<script type='text/javascript' src='$element' ></script>";#type='text/javascript'
			}
			$body->Push(implode($array));
		}
		echo $generator->Generate();
	}

	function GenComplexTag($tag, $push = null, $attr = null)
	{
		$tag = new TagComplexDecorator($tag, $attr);
		if($push !== null)
			$tag->Push($push);
		return $tag;
	}

	function GenHr()
	{
		return new TagDecorator('hr');
	}

	function GenInput($type, $val = '', $attr = null)
	{
		$input = new TagDecorator('input', $attr);
		$input->PushAttributes(['type' => $type, 'value' => $val]);
		return $input;
	}

	function GenImg($src, $alt, $attr = null)
	{
		$img = new TagDecorator('img', $attr);
		$img->PushAttributes(['src' => $src, 'alt' => $alt]);
		return $img;
	}

	/// @return HtmlGen\TagComplexDecorator
	function GenDiv($attr = null)
	{
		return new TagComplexDecorator('div', $attr);
	}

	function GenP($text, $attr = null)
	{
		return (new TagComplexDecorator('p', $attr))->StrictPush($text);
	}

	function GenA($text, $href, $attr = null)
	{
		$a = new TagComplexDecorator('a', $attr);
		$a->PushAttributes(['href' => $href]);
		return $a->StrictPush($text);
	}

	function GenMeta($name, $content)
	{
		return new TagDecorator('meta', ['name' => $name, 'content' => $content]);
	}

	function GenFavicon($path = '/favicon.ico')
	{
		return '<link rel="icon" type="image/vnd.microsoft.icon" href="'.$path.'" />';
	}

	function StringToHtmlAttrFormat($str)
	{
		return str_replace(['\'', '"'], ['&#039;', '&quot;'], $str);
	}
}