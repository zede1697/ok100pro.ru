<?php
/**
 * Created by PhpStorm.
 * User: Zede
 * Date: 11.01.2016
 * Time: 0:54
 */

require_once('Base.php');

function GetModule($name)
{
	$jsonStamp = new \JsonGen\JsonStamp(['status' => 'correct'], ['result']);
	switch($name)
	{
		case 'enter':
		{
			$module = HtmlGen\GenSectionEnter()->Generate();
			echo($jsonStamp->Generate([$module]));
		}break;
		case 'profile':
		{
			$module = HtmlGen\GenSectionProfile()->Generate();
			echo($jsonStamp->Generate([$module]));
		}break;
		case 'menu':
		{
			$module = HtmlGen\GenSectionMenu()->Generate();
			echo($jsonStamp->Generate([$module]));
		}break;
		case 'nav':
		{
			$module = HtmlGen\GenSectionNav()->Generate();
			echo($jsonStamp->Generate([$module]));
		}break;
		default:
		{
			die(\JsonGen\GenError('Nonexistent module'));
		}
	}
}

function ScriptExecute($name, $params)
{
	switch($name)
	{
		case 'download':
		{
			var_export($_FILES);
		}break;
		case 'getModule':
		{
			if(!(isset($params['name'])))
				die(\JsonGen\GenError('Error of params'));
			GetModule($params['name']);
		}
	}
}