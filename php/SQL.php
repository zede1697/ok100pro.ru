<?php

/**
 * Created by PhpStorm.
 * User: Zede
 * Date: 07.01.2016
 * Time: 3:55
 */

class SQLWorker
{
	static protected $mysqli; //object mysqli
	static protected $status; //bool

	protected $params; //array

	protected $query;


	public static function ActivateSQL($site, $user, $password, $database)
		{
			if(self::$status)
				return;
			self::$mysqli = new mysqli($site, $user, $password, $database);
			self::$status = true;
		}

	public static function DeactivateSQL()
		{
			if(!self::$status)
				return;
			self::$mysqli->close();
			self::$status = false;
		}

	public static function EscapeString($string)
		{
			return self::$mysqli->real_escape_string($string);
		}


	protected function DecorateTable()
		{
			return "`{$this->params['table']}`";
		}

	protected function DecorateColumns()
		{
			$decorated = '';
			foreach($this->params['columnsArray'] as $column)
				$decorated .= "`$column`, ";
			$decorated = substr($decorated, 0, -2);
			return $decorated;
		}

	protected function DecorateValues()
	{
		$decorated = "(";
		foreach($this->params['valuesArray'] as $value)
			$decorated .= ($value === NULL)?"NULL, " : "'$value', " ;
		$decorated = substr($decorated, 0, -2);
		return $decorated . ")";
	}

	protected function DecorateExpression()
		{
			return "({$this->params['expression']})";
		}


	public function __construct()
		{
			if(!self::$status)
				throw new Exception('Not activated SQL!');
		}


	public function SetTable($tableNew)
		{
			$this->params['table'] = self::EscapeString($tableNew);
		}

	public function SetColumnsArray($columnsArrayNew = [])
		{
			if($columnsArrayNew == [])
			{
				unset($this->params['columnsArray']);
				return;
			}
			foreach($columnsArrayNew as $column)
				$column = self::EscapeString($column);
			$this->params['columnsArray'] = $columnsArrayNew;
		}

	public function SetValuesArray($valuesArrayNew = [])
		{
			if($valuesArrayNew == [])
			{
				unset($this->params['valuesArray']);
				return;
			}
			foreach($valuesArrayNew as $value)
				$column = self::EscapeString($value);
			$this->params['valuesArray'] = $valuesArrayNew;
		}

	public function SetExpression($expressionNew = '', $arrayOfParams = [])
		{
			if($expressionNew == '')
			{
				unset($this->params['expression']);
				return;
			}
			$replacer = [];
			$id = 0;
			foreach($arrayOfParams as $param)
			{
				$param = self::EscapeString($param);
				$replacer[$id] = '%'.($id++).'%';
			}
			$this->params['expression'] = str_replace($replacer, $arrayOfParams, $expressionNew);
		}

	public function SetAssignment($assignmentsNew = '')
		{
			if($assignmentsNew == '')
			{
				unset($this->params['assignment']);
				return;
			}
			$result = '';
			foreach($assignmentsNew as $key => $val)
				$result .= '`'.self::EscapeString($key).'` = '.var_export(self::EscapeString($val), true).', ';
			$this->params['assignment'] = substr($result, 0, -2);
		}

	public function SetSorting($columnName ,$sorted = true, $ascending = true)
		{
			if($sorted)
				$this->params['sortString'] = "GROUP BY `$columnName ` ".(($ascending) ? 'ASC' : 'DESC');
			else
				unset($this->params['sortString']);
		}

	public function SetLimit($rows, $offset = 0)
		{
			if($rows !== 0)
				$this->params['limitString'] = "LIMIT ".(($offset !== 0) ? ($offset.', ') : '').$rows;
			else
				unset($this->params['limitString']);
		}


	public function GetLastQueryString()
		{
			return $this->query;
		}

	public function ExecuteQuery()
		{
			return self::$mysqli->query($this->query);
		}

	public function ClearParams()
		{
			unset($this->params);
		}

	public function GetError()
		{
			return self::$mysqli->error;
		}


	public function Query_Insert()
		{
			$this->query = 'INSERT INTO '.$this->DecorateTable().'('.$this->DecorateColumns().')'.
				' VALUES '.$this->DecorateValues().';';
			return $this->ExecuteQuery();
		}

	public function Query_Exist()
		{
			$this->query = 'SELECT EXISTS (SELECT * FROM '.$this->DecorateTable().
				' WHERE ('.$this->params['expression'].'))';
			$result = $this->ExecuteQuery()->fetch_array();
			return (is_array($result))? $result[0] : false;
		}

	public function Query_Update()
		{
			$this->query = 'UPDATE '.$this->DecorateTable().' SET '.$this->params['assignment'].
				' WHERE ('.$this->params['expression'].')';
			$this->ExecuteQuery();
		}

	public function Query_Select($distinctValue = false)
		{
			$this->query = 'SELECT '.(($distinctValue)?'DISTINCT':'').                               //distinct
				((isset($this->params['columnsArray'])) ? ($this->DecorateColumns()) : '* ').            //columns [* | array]
				' FROM '.$this->DecorateTable().                                                        //from
				((isset($this->params['expression'])) ? (' WHERE ('.$this->params['expression'].')') : '').//where [| array]
				((isset($this->params['sortString'])) ? $this->params['sortString'] : '').             //order by
				((isset($this->params['limitString'])) ? $this->params['limitString'] : '');           //limit
			$result = $this->ExecuteQuery();
			return $result->fetch_all();
		}
}

class SQLUsers
{
	protected $SQLWorker;

	static protected function EncryptionPassword($password)
		{
			return crypt($password, substr($password, 0, -2));
		}

	public function __construct()
	{
		$this->SQLWorker = new SQLWorker();
		$this->SQLWorker->SetTable('users');
		$this->SQLWorker->SetColumnsArray(
				['id', 'login', 'email', 'password', 'name', 'surname', 'vehicle', 'phone', 'status', 'cash']
			);
	}

	public function Registration($login, $email, $password, $name, $surname, $vehicle, $phone, $status, $cash)
		{
			$this->SQLWorker->SetValuesArray(
				[NULL, $login, $email, self::EncryptionPassword($password), $name, $surname, $vehicle, $phone, $status, $cash]);
			$this->SQLWorker->Query_Insert();
			return ($this->SQLWorker->GetError())? false : true;
			//echo $this->SQLWorker->GetLastQueryString();
		}

	public function Check($email, $password)
		{
			$this->SQLWorker->SetExpression('`email` = "%0%" AND `password` = "%1%"',
				[$email, self::EncryptionPassword($password)]);
			return $this->SQLWorker->Query_Exist();
		}

	public function CheckExistByEmail($email)
	{
		$this->SQLWorker->SetExpression('`email` = "%0%"', [$email]);
		return $this->SQLWorker->Query_Exist();
	}

	public function GetUserById($id)
		{
			$this->SQLWorker->SetExpression('`id` = "%0%"', [$id]);
			return $this->SQLWorker->Query_Select()[0];
		}

	public function GetUserId($email)
		{
			$this->SQLWorker->SetExpression('`email` = "%0%"', [$email]);
			return $this->SQLWorker->Query_Select()[0];
		}
}

class SQLStrings
{
	protected $SQLWorker;

	public function __construct()
		{
			$this->SQLWorker = new SQLWorker();
			$this->SQLWorker->SetTable('strings');
		}

	public function AddString($key, $val)
		{
			$this->SQLWorker->SetColumnsArray(['id', 'keystr', 'value']);
			$this->SQLWorker->SetValuesArray([NULL, $key, $val]);
			$this->SQLWorker->Query_Insert();
			echo $this->SQLWorker->GetLastQueryString();
		}

	public function GetString($key)
		{
			$this->SQLWorker->SetColumnsArray(['value']);
			$this->SQLWorker->SetExpression('`keystr` = "%0%"', [$key]);
			return $this->SQLWorker->Query_Select()[0];
		}

	public function SetString($key, $value)
		{
			$this->SQLWorker->SetAssignment(["value" => $value]);
			$this->SQLWorker->SetExpression('`keystr` = "%0%"',[$key]);
			$this->SQLWorker->Query_Update();
			echo $this->SQLWorker->GetLastQueryString();
		}

	public function CheckExistsKey($key)
		{
			$this->SQLWorker->SetExpression('`keystr` = "%0%"', [$key]);
			return $this->SQLWorker->Query_Exist();
		}
}
