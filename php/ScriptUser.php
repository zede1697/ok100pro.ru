<?php

/**
 * Created by PhpStorm.
 * User: Zede
 * Date: 10.01.2016
 * Time: 19:09
 */

require_once('Base.php');

function LogIn($email, $password)
{
	$sqlUser = new SQLUsers();
	if($sqlUser->Check($email, $password))
	{
		$userId = $sqlUser->GetUserId($email);
		Session::Initialization($userId);
		echo(json_encode(['status' => 'correct', 'result' => 'true']));
	}else
		echo(json_encode(['status' => 'correct', 'result' => 'false']));
}

function LogOut()
{
	Session::Reset();
	echo json_encode(['status' => 'correct', 'result' => '']);
}

function SignUp($email, $password, $login, $name, $surname, $auto, $phone)
{
	$sqlUser = new SQLUsers();
	if($sqlUser->CheckExistByEmail($email))
		die(json_encode(['status' => 'correct', 'result' => 'false', 'message' => 'Данный email уже зарегестрирован']));
	$result = $sqlUser->Registration($login, $email, $password, $name, $surname, $auto, $phone, '0', '0');
	if($result)
	{
		$subject = 'Регистрация на сайте ok100pro.ru';
		$message = <<<'HTML'
<html>
	<head>
		<title>Регистрация на сайте ok100pro.ru</title>
	</head>
	<body>
		TestMessage!
	</body>
</html>
HTML;
		$message = wordwrap($message, 70, "\r\n");
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		$headers .= 'From: 	100proexpertiza@ok100pro.ru'."\r\n";
		if(!mail($email, $subject, $message, $headers))
			die(json_encode(['status' => 'correct', 'result' => 'false', 'message' => 'Ошибка отправки сообщения на почту']));
	}
	echo(json_encode(['status' => 'correct', 'result' => ($result)? 'true' : 'false']));
}

function ScriptExecute($name, $params)
{
	switch($name)
	{
		case 'logIn': //params 'email', 'password'
		{
			if(!(isset($params['password']) && isset($params['email'])))
				die(json_encode(['status' => 'error', 'message' => 'Error of params']));
			LogIn($params['email'], $params['password']);
		}break;
		case 'logOut': //params void
		{
			LogOut();
		}break;
		case 'signUp':
		{
			if(!all_is_set(['login', 'email', 'password', 'name', 'surname', 'phone', 'auto', 'captcha'], $params))
				die(json_encode(['status' => 'error', 'message' => 'Error of params']));
			$myCurl = curl_init();
			$postParams = http_build_query(['secret' => '6Ld3lBcTAAAAAHx4gzBtRH7Z6KduLSkqlBbw_kkS', 'response' => $params['captcha']]);
			curl_setopt_array($myCurl,[
					CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_POST => true,
					CURLOPT_POSTFIELDS => $postParams
				]);
			$response = curl_exec($myCurl);
			curl_close($myCurl);
			if(json_decode($response, true)['success'] === true)
				SignUp($params['email'], $params['password'], $params['login'], $params['name'], $params['surname'], $params['auto'], $params['phone']);
			else
				die(json_encode(['status' => 'correct', 'result' => 'false']));
		}break;
		default:
			die(json_encode(['status' => 'error', 'message' => 'No existed method']));
	}
}