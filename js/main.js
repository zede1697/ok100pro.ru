/**
 * Created by Zede on 15.01.2016.
 */

var
	sys = {};

window.onload = function()
	{
		//<editor-fold desc="Object sys init">
		//<editor-fold desc="Basic definitions">
		sys.defBool = true;
		sys.defNum = 0;
		sys.defStr = '';
		sys.defArray = [];
		sys.defObject = {};
		sys.defFunction = function(){};
		sys.isUndefined = function(a){return typeof(a) === "undefined"};
		sys.isNumber = function(a){return typeof(a) === "number";};
		sys.isString = function(a){return typeof(a) === "string";};
		sys.isObject = function(a){return typeof(a) === "object";};
		sys.isArray = function(a){return typeof(a) === "object" && Array.isArray(a);};
		sys.isFunction = function(a){return typeof(a) === "function"};
		sys.returnFalse = function(){return false;};
		sys.onMouseUp = function(e){sys.linkClick(e.target.getAttribute('href'), e.which);};
		(sys.customize = function()
			{
				var
					links = $('a[href^="/page/"]:not([href^="#"])');
				links.mouseup(sys.onMouseUp);
				$('a[href^="#"]').each(function(_, e)
					{
						e.setAttribute('href', location.pathname + e.getAttribute('href'));
					});
				links.click(sys.returnFalse);
			})();
		sys.linkClick = function(href, button)
			{
				if(button ==1)
				{
					if(href[0] == '/')
					{
						$.post(href, {onlyContent: 'true'}, function(res)
							{
								var obj = JSON.parse(res);
								document.getElementsByTagName('main')[0].innerHTML = obj.content;
								document.title = obj.title;
								if(obj.script !== undefined)
									(new Function('loaded', 'style', obj.script))(true, obj.style);
								history.pushState({title: res.title, href: href}, res.title, href);
								sys.customize();
								sys.menu.close();
							});
					}else
					{

					}
				}else if(button == 2)
				{
					window.open(href, '_blank');
					window.focus();
				}
			};
		//</editor-fold>

		//<editor-fold desc="Object sys.menu inti">
		sys.menu = {};
		sys.menu.currentModule = null;
		sys.menu.panel = {};
		sys.menu.panel.header = {};
		sys.menu.panel.form = {};
		sys.menu.tag = document.getElementById('main-menu');
		sys.menu.panel.tag = sys.menu.tag.firstElementChild;
		sys.menu.panel.header.tag =  $('#main-menu > div > header')[0];
		sys.menu.panel.header.textTag =  $('#main-menu > div > header > p')[0];
		sys.menu.panel.form.tag = $('#main-menu > div > div')[0];
		sys.menu.open = function(key)
			{
				var
					baseQuery = '#data > div[data-key="' + key + '"]',
					target = $(baseQuery)[0];
				if(target)
				{
					sys.menu.currentModule = sys.modules[key];
					sys.menu.tag.setAttribute('data-type', 'horizontal');
					sys.menu.panel.header.textTag.innerHTML = $(baseQuery + '> header')[0].innerHTML;
					sys.menu.panel.form.tag.setAttribute('data-module', key);
					sys.menu.panel.form.tag.innerHTML = $(baseQuery + '> div')[0].innerHTML;
					sys.customize();
					sys.menu.currentModule.space = sys.menu.panel.form.tag;
					sys.menu.currentModule.onopened();
				}
			};
		sys.menu.close = function()
			{
				if(sys.menu.currentModule == null)
					return;
				sys.menu.panel.tag.style.width = '0';
				sys.menu.tag.style.backgroundColor = 'transparent';
				setTimeout(function()
				 {
					 sys.menu.currentModule.onclose();
					 sys.menu.tag.removeAttribute('data-type');
					 sys.menu.tag.removeAttribute('style');
					 sys.menu.panel.tag.removeAttribute('style');
				 }, 500);
			};
		//</editor-fold>

		//<editor-fold desc="Object sys.header init">
		sys.header = {};
		sys.header.tag = $('body > header')[0];
		sys.header.nav = {};
		sys.header.nav.tag = document.getElementById('headerNav');
		(sys.header.nav.CalcBottomPos = function()
			{
				sys.header.nav.tag.bottomPos = sys.header.tag.getBoundingClientRect().bottom + window.pageYOffset - 44;
			})();
		sys.header.nav.submenuTag = document.querySelector('*[data-type="sub-menu"]');
		sys.header.nav.submenuLinkTag = document.querySelector('*[data-access="sub-menu"]');
		sys.header.nav.submenuLinkTag.onmouseover = sys.header.nav.submenuTag.onmouseover =
			function(){sys.header.nav.submenuTag.setAttribute('data-opened', '');};
		sys.header.nav.submenuLinkTag.onmouseout = sys.header.nav.submenuTag.onmouseout =
			function(){sys.header.nav.submenuTag.removeAttribute('data-opened');};
		//</editor-fold>

		//<editor-fold desc="Object sys.modules">
		sys.modules = {};
		sys.modules.Module = function(name)
			{
				this.name = name;
				this.onquery = sys.defFunction;
				this.onopened = sys.defFunction;
				this.onclose = sys.defFunction;
				this.data = {};
				this.space = null;
			};
		sys.modules.load = function(data, callback)
			{
				if(sys.isObject(data) && data.className === 'data-js')
				{
					var
						parentTag = data.parentNode,
						jsCode = data.getAttribute('value'),
						fabric = new Function(jsCode);
					sys.modules[parentTag.getAttribute('data-key')] = fabric();
				}else if(sys.isString(data))
				{
					sys.script('files.getModule',{name: data}, function(res)
						{
							var
								dataTag = document.getElementById('data'),
								response = JSON.parse(res);
							if(response.status === "correct")
							{
								dataTag.innerHTML += response.result;
								sys.modules.load(dataTag.querySelector('[data-key=' + data + '] .data-js'));
								callback();
							}else
								console.log('error:' + response.result);
						});
				}
			};
		//</editor-fold>

		sys.script = function(name, params, callback)
			{
				if(sys.isString(name) && sys.isObject(params))
				{
					$.post('script/' + name, {jsonParams: JSON.stringify(params)}, function(res)
						{
							if(sys.isFunction(callback))
								callback(res);
						});
				}
			};

		sys.state = JSON.parse(document.getElementById('data-json').value);
		document.body.setAttribute('data-init', sys.state.session_active.toString());


		$('#data > div[data-key] > input.data-js').each(function(id, moduleTag)
			{
				sys.modules.load(moduleTag);
			});
		//sys.modules.load($('[data-key=enter]>input.data-js')[0]);
		//</editor-fold>

		//<editor-fold desc="Setting handler and other">
		//неперетаскиваемые сущности
		$('.no-drag').each(function()
			{
				$(this).bind('dragstart', function(e)
					{
						if(window.event) event.preventDefault();
							e.cancelBubble = true;
						return false;
					});
			});

		$('#headerNav > ul > li').mouseup(function(e)
			{
				var
					moduleName = e.target.getAttribute('data-access');
				if(moduleName === 'none')
					return;
				if(!sys.isUndefined(sys.modules[moduleName]))
					sys.modules[moduleName].onquery(e);
				else
				{
					sys.modules.load(moduleName, function()
						{
							sys.modules[moduleName].onquery();
						});
				}
			});

		var
			links = $('a[href|="/page/"]');
		links.mouseup(sys.onMouseUp);
		links.click(sys.returnFalse);

		$('#main-menu').click(function(e)
			{
				if(e.target !== sys.menu.tag)
					return;
				sys.menu.close();
			});

		$(sys.menu.panel.tag).on('swipeLeft',function(){sys.menu.close();});
		$('#main-menu > div > header > button')[0].onclick = sys.menu.close;

		$(document.body).on('swipeRight',function()
			{
				if(!sys.menu.tag.hasAttribute('data-type'))
					if(!sys.isUndefined(sys.modules['nav']))
						sys.modules['nav'].onquery();
					else
						sys.modules.load('nav', function()
							{
								sys.modules['nav'].onquery();
							});
			});

		window.onscroll = function()
			{
				var
					isContainFixed = sys.header.nav.tag.classList.contains('fixed');
				if (isContainFixed && window.pageYOffset < sys.header.nav.tag.bottomPos)
				{
					sys.header.nav.tag.classList.remove('fixed');
					sys.header.tag.style.paddingBottom = '0';
				}else if (!isContainFixed && window.pageYOffset > sys.header.nav.tag.bottomPos)
				{
					sys.header.nav.tag.classList.add('fixed');
					sys.header.tag.style.paddingBottom = '42px';
				}
			};

		(window.onresize = function()
			{
				sys.header.nav.CalcBottomPos();
				window.onscroll();
			})();

		window.onpopstate = function(e)
			{
				var state = JSON.stringify(e.state);
				$.post(state.href, {onlyContent: 'true'}, function(res)
					{
						var obj = JSON.parse(res);
						document.getElementsByTagName('main')[0].innerHTML = obj.content;
						document.title = obj.title;
						sys.customize();
						sys.menu.close();
					});
			};

		history.replaceState({title: document.title, href : location.pathname}, document.title);

		//убираем загрузку
		document.body.removeChild(document.getElementById('loadingPanel'));
		document.body.removeAttribute('data-loading');
		//</editor-fold>
	};