<?php
/**
 * Created by PhpStorm.
 * User: Zede
 * Date: 08.01.2016
 * Time: 3:18
 */

if(isset($_GET['name']))
{
	include_once('php/Base.php');
	if(!isset($_POST['jsonParams']))
		die(\JsonGen\GenError('No POST "jsonParams" param'));
	$array = explode('.', $_GET['name']);
	$section = $array[0];
	$name = $array[1];
	switch($section)
	{
		case 'user':
		{
			/*
			 * user.logIn (email: string, password: string) -> [true | false]
			 * user.logOut () -> void
			 * user.signUp (email: string, password: string, )
			 */
			include_once('php/ScriptUser.php');
		}break;
		case 'files':
		{
			/*
			 * files.download (file: file) -> [true | false]
			 * files.getModule (name: string) -> (HtmlCode):string
			 */
			include_once('php/ScriptFile.php');
		}break;
		default:
			die(JsonGen\GenError('No existed section'));
	}
	ScriptExecute($name, json_decode($_POST['jsonParams'], true));
}else
	die(JsonGen\GenError('No GET "name" param'));