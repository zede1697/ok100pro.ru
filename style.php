<?php
/**
 * Created by PhpStorm.
 * User: Zede
 * Date: 11.01.2016
 * Time: 21:09
 */
header('Content-Type:text/css; charset=UTF-8');
if(strpos($_GET['name'], '.') !== false)
{
	$fileName = $_GET['name'];
}else
{
	$fileName = 'style/' . $_GET['name'] . '.less';
	$fileName = (file_exists($fileName)) ? $fileName : 'style/' . $_GET['name'] . '.css';
}
echo file_get_contents($fileName);