<?php
/**
 * Created by PhpStorm.
 * User: Zede
 * Date: 24.01.2016
 * Time: 4:26
 */

require_once 'php/Base.php';

$pageList = json_decode(file_get_contents('./page/pages.json'), true);
$settings = json_decode(file_get_contents('settings.json'), true);
$currentPage = $_GET['name'];
if($settings['repairs'])
	$currentPage = 'global_repairs';
echo $currentPage;
if(isset($pageList[$currentPage]))
	include_once('page/'.$pageList[$currentPage]);
else
	include_once('page/404.php');

if(isset($_POST['onlyContent']))
{
	$json = ['title' => PageTitle(), 'content' => PageContent()];
	if(function_exists('PageScript'))
		$json = array_merge($json, ['script' => PageScript()]);
	if(function_exists('PageStyle'))
		$json = array_merge($json, ['style' => 'style/' . PageStyle()]);
	echo json_encode($json, JSON_FORCE_OBJECT);
}else
{
	$head = new HtmlGen\TagHead(PageTitle());
	$head->StrictPush(['<base href="/"/>', HtmlGen\GenMeta('viewport', 'width=device-width, initial-scale=1'), HtmlGen\GenFavicon()]);
	$head->PushCss(['base.css', 'main.css', 'menu-modules.css']);

	$body = new HtmlGen\TagBody(['data-loading' => 'true']);
	$formData = HtmlGen\GetOwnDataForm(['title' => 'Test page']);
	$body->StrictPush([HtmlGen\GenLoadingPanel(), HtmlGen\GenMenuPanel()]);
	$mainLine = HtmlGen\GenOwnMainLine();
	$mainLine->StrictPush([PageContent(), HtmlGen\GenOwnFooter()]);
	$body->StrictPush([HtmlGen\GenOwnHeader(), $mainLine, $formData]);
	if(function_exists('PageScript'))
	{
		$script = new \HtmlGen\TagComplexDecorator('script');
		$script->StrictPush(PageScript());
		$body->StrictPush($script);
	}
	if(function_exists('PageStyle'))
	{
		$style = new \HtmlGen\TagComplexDecorator('link', ['rel' => 'stylesheet', 'href' => ('style/' . PageStyle())]);
		$body->StrictPush($style);
	}
	HtmlGen\DocumentFabric($head, $body, ['zepto.min.js', 'main.js']);
	SQLWorker::DeactivateSQL();
}