<?php
/**
 * Created by PhpStorm.
 * User: Zede
 * Date: 03.02.2016
 * Time: 20:01
 */

function PageTitle()
{
	return 'ok100pro: палата экспертов';
}

function PageContent()
{
	$div = \HtmlGen\GenDiv();
	$div->StrictPush('Палата экспертов (независимых судей):');
	$liStamp = new \HtmlGen\TagStamp('li');
	$ul = new \HtmlGen\TagComplexDecorator('ul');
	$ul->StrictPush($liStamp->ComplexGenerate([
			[[], HtmlGen\GenA('Эксперты-техники', '#')],
			[[], HtmlGen\GenA('Судебные эксперты', '#')],
			[[], HtmlGen\GenA('Эксперты криминалисты', '#')],
			[[], HtmlGen\GenA('Эксперт-трассолог (трассологический каббинет)', '#')]
		]));
	$div->StrictPush([$ul, 'Возможность получение ответа независимой экспертизы в любой точке России по фиксированным тарифам<hr/>']);
	return $div->Generate();
}