<?php
/**
 * Created by PhpStorm.
 * User: Zede
 * Date: 09.01.2016
 * Time: 13:25
 */
if(!isset($_POST['onlyContent']))
	header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');

function PageTitle()
{
	return 'Ошибка: файл не найден';
}

function PageContent()
{
	return '<p>На данном сайте не существует запрошенного ресурса: '.$_SERVER['REQUEST_URI'].'</p>';
}