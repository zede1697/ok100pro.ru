<?php
/**
 * Created by PhpStorm.
 * User: zede
 * Date: 26.03.2016
 * Time: 2:12
 */

//require_once('../php/Parsedown.php');


function PageTitle()
{
	return 'ok100pro: ремонтные работы';
}

function PageContent()
{
	return <<<HTML
<style>

</style>
<div class="screen-fix">
	<!--<img src="images/material-background.svg" style="width: 100%;height: 100%;position: absolute;z-index:-1;">-->
	<div style="background-color: #202020;"></div>
	<div style="display: flex;width: 100%;height: 100%;justify-content:center;align-items:center;flex-direction:column;">
		<img src="images/logo.png" style="max-width: 50vmin;height: auto;display: block"/>
		<div style="max-width: 600px;font-size: 1.2em;margin-top:5px;padding:8px">
			Приносим свои извинения,<br/>
			на данный момент на сайте ведутся технические работы. Мы делаем все возможное для того, чтобы Вам было максимально удобно пользоваться нашим ресурсом.<br/>
			C уважением, команда <b>100PRO</b>.
		</div>
	</div>
</div>
HTML;
}