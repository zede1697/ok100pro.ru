<?php
/**
 * Created by PhpStorm.
 * User: Zede
 * Date: 24.01.2016
 * Time: 4:57
 */

function PageTitle()
{
	return 'ok100pro: регистрация';
}

function PageContent()
{
	$page = new \HtmlGen\TagComplexDecorator('form', ['id' => 'reg-form']);
	$inputs = new \HtmlGen\TagStamp('input', ['class' => '-reg-text'], ['id', 'type', 'value']);
	$page->StrictPush([
			\HtmlGen\GenComplexTag('h1', 'Регистрация нового пользщователя:'),
			'Почтовый ящик:<br class=\'-reg-wrap\'/>',
			$inputs->SGenerate([['reg-email', 'email', '']]),
			'<br/>Пароль:<br class=\'-reg-wrap\'/>',
			$inputs->SGenerate([['reg-password', 'password', '']]),
			'<br/>Логин:<br class=\'-reg-wrap\'/>',
			$inputs->SGenerate([['reg-login', 'text', '']]),
			'<br/>Имя:<br class=\'-reg-wrap\'/>',
			$inputs->SGenerate([['reg-name', 'text', '']]),
			'<br/>Фамилия:<br class=\'-reg-wrap\'/>',
			$inputs->SGenerate([['reg-surname', 'text', '']]),
			'<br/>Телефон:<br class=\'-reg-wrap\'/>',
			$inputs->SGenerate([['reg-phone', 'tel', ''], 'pattern' => '\+7\([0-9]{3}\)-[0-9]{3}']),
			'<br/>Автомобильный номер:<br class=\'-reg-wrap\'/>',
			$inputs->SGenerate([['reg-auto', 'text', '']]),
			'<br/><div class=\'g-recaptcha\' data-theme="dark" data-size="compact" data-sitekey=\'6Ld3lBcTAAAAANlwM0kYvlOLqLDX54AAzV0m7Tw6\'></div>',
			'<div id=\'reg-output\'></div>',
			\HtmlGen\GenInput('button', 'Зарегистрироваться', ['id' => 'reg-button']),
			'<script src="https://www.google.com/recaptcha/api.js"></script>'
		]);
	return $page->Generate();
}

function PageScript()
{
	return <<<'JS'
var
	emailTag = document.getElementById('reg-email'),
	passwordTag = document.getElementById('reg-password'),
	loginTag = document.getElementById('reg-login'),
	nameTag = document.getElementById('reg-name'),
	surnameTag = document.getElementById('reg-surname'),
	phoneTag = document.getElementById('reg-phone'),
	autoTag = document.getElementById('reg-auto'),
	submitTag = document.getElementById('reg-button'),
	outputTag = document.getElementById('reg-output'),
	inputOnChange = function(e)
		{
			console.log(e.target.validity);
		};
emailTag.oninput = passwordTag.oninput = loginTag.oninput =
	phoneTag.oninput = autoTag.oninput = inputOnChange;
submitTag.onclick = function()
	{
		var captchaResponse = grecaptcha.getResponse();
		if(captchaResponse === "")
		{
			outputTag.innerHTML = 'Подтвердите, что вы не робот';
			return;
		}
		var obj = {
				email: emailTag.value, password: passwordTag.value, login: loginTag.value,
				name: nameTag.value, surname: surnameTag.value, phone: phoneTag.value, auto: autoTag.value,
				captcha: captchaResponse
			};
		sys.script('user.signUp', obj, function(res)
			{
				var html, resObj = JSON.parse(res);
				if(resObj.result === "true")
					html = 'Регистрация аккаунта прошла успешно.<br/> Закончите регистрацию перейдя по ссылке отправленной вам в сообщении на ваш email';
				else
					html = (sys.isUndefined(resObj.message))?'Произошла ошибка. Проверьте правильность заполненных полей' : resObj.message;
				outputTag.innerHTML = html;
				grecaptcha.reset();
			});
	};
if(typeof(loaded) !== 'undefined')
	{
		var captchaScript = document.createElement('script');
		captchaScript.type = 'text/javascript';
		captchaScript.async = true;
		captchaScript.src = document.querySelector('#reg-form > script').getAttribute('src');
		document.body.appendChild(captchaScript);
	}
if((typeof(style) !== 'undefined') && ($('link[href$="' + style +'"]').length === 0))
	{
		var styleTag = document.createElement('link');
		styleTag.setAttribute('rel', 'stylesheet');
		styleTag.setAttribute('href', style);
		document.body.appendChild(styleTag);
	}
JS;
}

function PageStyle()
{
	return 'page/registration.css';
}